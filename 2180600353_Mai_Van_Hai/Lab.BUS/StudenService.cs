﻿using Lab.DAL.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab.BUS
{
    internal class StudenService
    {
        public List<SinhVien> GetAll()
        {
            StudentModel context = new StudentModel();
            return context.SinhViens.ToList();

        }
        
        public SinhVien FindBy(string studenId)
        {
            StudentModel context = new StudentModel();
            return context.SinhViens.FirstOrDefault(p => p.MaSV == studenId);
        }
        public void InsertUpdate(SinhVien s)
        {
            StudentModel context = new StudentModel();
            context.SinhViens.AddOrUpdate(s);
            context.SaveChanges();
        }
        public bool checkMSSV(string id)
        {
            StudentModel context = new StudentModel();
            var list = context.SinhViens.ToList();
            foreach (var i in list)
            {
                if (i.MaSV == id)
                {
                    return true;
                }
            }
            return false; // Thêm dòng này để trả về giá trị mặc định
        }
        public bool checkInFo(string ma, string ten, string dtb)
        {
            if (ma == "" || ten == "" || dtb == "")
            {
                return false;
            }
            return true;
        }
    }
}
