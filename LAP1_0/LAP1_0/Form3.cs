﻿
using LAP1_0.Models;
using LAP1_0;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LAP1_0
{
    public partial class Form3 : Form
    {
        public bool showForm1OnClose = false;
        StudentContextDB db;
        private List<string> facultyNamesFromForm1;
        private int searchResultCount = 0;

        public Form3(List<string> facultyNamesFromForm1)
        {
            InitializeComponent();
            db = new StudentContextDB();
            this.facultyNamesFromForm1 = facultyNamesFromForm1;
        }

        private void SearchStudents()
        {
            string studentID = txtStudentID.Text.Trim();
            string fullName = txtFullname.Text.Trim();
            string facultyName = cmbFaculty.Text;

            // Lọc danh sách sinh viên theo các điều kiện
            var query = db.Students.AsQueryable();

            if (!string.IsNullOrEmpty(studentID))
            {
                query = query.Where(student => student.StudentID.Contains(studentID));
            }

            if (!string.IsNullOrEmpty(fullName))
            {
                query = query.Where(student => student.FullName.Contains(fullName));
            }

            if (!string.IsNullOrEmpty(facultyName))
            {
                query = query.Where(student => student.Faculty.FacultyName == facultyName);
            }

            List<Student> filteredStudents = query.ToList();

            // Đếm số lượng kết quả
            searchResultCount = filteredStudents.Count;

            // Hiển thị kết quả tìm kiếm trên DataGridView hoặc thông báo nếu không có kết quả
            if (searchResultCount > 0)
            {
                dgvTimKiem.Rows.Clear();
                foreach (Student student in filteredStudents)
                {
                    int index = dgvTimKiem.Rows.Add();
                    dgvTimKiem.Rows[index].Cells[0].Value = student.StudentID;
                    dgvTimKiem.Rows[index].Cells[1].Value = student.FullName;
                    dgvTimKiem.Rows[index].Cells[2].Value = student.Faculty.FacultyName;
                    dgvTimKiem.Rows[index].Cells[3].Value = student.AverageScore;
                }
            }
            else
            {
                MessageBox.Show("Không có kết quả nào phù hợp với điều kiện tìm kiếm.", "Thông báo", MessageBoxButtons.OK);
            }

            // Cập nhật số lượng kết quả vào txtketquatim_TextChanged
            txtketquatim.Text = $"{searchResultCount}";
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            SearchStudents();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            string studentIDToDelete = txtStudentID.Text.Trim();
            string fullNameToDelete = txtFullname.Text.Trim();
            string facultyToDelete = cmbFaculty.Text;

            // Kiểm tra xem có dữ liệu để xóa hay không
            if (string.IsNullOrEmpty(studentIDToDelete) && string.IsNullOrEmpty(fullNameToDelete) && string.IsNullOrEmpty(facultyToDelete))
            {
                MessageBox.Show("Vui lòng nhập thông tin để xóa sinh viên.", "Thông báo", MessageBoxButtons.OK);
                return;
            }
            // Tiến hành kiểm tra và xóa sinh viên
            DeleteStudent(studentIDToDelete, fullNameToDelete, facultyToDelete);
        }
        private void DeleteStudent(string studentIDToDelete, string fullNameToDelete, string facultyToDelete)
        {
            // Tìm sinh viên dựa trên thông tin nhập vào
            List<Student> studentsToDelete = db.Students
                .Where(s =>
                    (string.IsNullOrEmpty(studentIDToDelete) || s.StudentID == studentIDToDelete) &&
                    (string.IsNullOrEmpty(fullNameToDelete) || s.FullName == fullNameToDelete) &&
                    (string.IsNullOrEmpty(facultyToDelete) || s.Faculty.FacultyName == facultyToDelete))
                .ToList();

            if (studentsToDelete.Count > 0)
            {
                // Hiển thị xác nhận trước khi xóa
                DialogResult result = MessageBox.Show($"Bạn có chắc chắn muốn xóa ?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (result == DialogResult.Yes)
                {
                    // Xóa sinh viên khỏi cơ sở dữ liệu
                    foreach (var studentToDelete in studentsToDelete)
                    {
                        db.Students.Remove(studentToDelete);
                    }
                    db.SaveChanges();

                    // Xóa các dòng khỏi DataGridView
                    foreach (var studentToDelete in studentsToDelete)
                    {
                        foreach (DataGridViewRow row in dgvTimKiem.Rows)
                        {
                            if (studentToDelete.StudentID == row.Cells[0].Value.ToString())
                            {
                                dgvTimKiem.Rows.Remove(row);
                                break;
                            }
                        }
                    }

                    // Cập nhật số lượng kết quả và hiển thị
                    searchResultCount -= studentsToDelete.Count;
                    txtketquatim.Text = searchResultCount.ToString();

                    MessageBox.Show($"Đã xóa sinh viên thành công!", "Thông báo", MessageBoxButtons.OK);

                    // Gọi phương thức ReloadData từ Form1 để cập nhật dữ liệu trên DataGridView của Form1
                    Form1 form1 = Application.OpenForms.OfType<Form1>().FirstOrDefault();
                    if (form1 != null)
                    {
                        form1.ReloadData();
                    }
                }
            }
            else
            {
                MessageBox.Show("Không tìm thấy sinh viên để xóa!", "Thông báo", MessageBoxButtons.OK);
            }
        }

        private void btntrove_Click(object sender, EventArgs e)
        {
            showForm1OnClose = true;
            this.Close();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            cmbFaculty.Items.AddRange(facultyNamesFromForm1.ToArray());
            txtketquatim.Text = "0";
        }

        private void txtStudentID_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblketquatim_Click(object sender, EventArgs e)
        {

        }

        private void dgvTimKiem_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cmbFaculty_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void txtFullname_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtketquatim_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblkhoa_Click(object sender, EventArgs e)
        {

        }

        private void lblhoten_Click(object sender, EventArgs e)
        {

        }

        private void lblmasv_Click(object sender, EventArgs e)
        {

        }

        private void lblQuanli_Click(object sender, EventArgs e)
        {

        }
    }
}