﻿namespace LAP1_0
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtketquatim = new System.Windows.Forms.TextBox();
            this.lblketquatim = new System.Windows.Forms.Label();
            this.btntrove = new System.Windows.Forms.Button();
            this.dgvTimKiem = new System.Windows.Forms.DataGridView();
            this.colMSSV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colHoTen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colKhoa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDTB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnXoa = new System.Windows.Forms.Button();
            this.btnTim = new System.Windows.Forms.Button();
            this.cmbFaculty = new System.Windows.Forms.ComboBox();
            this.txtFullname = new System.Windows.Forms.TextBox();
            this.txtStudentID = new System.Windows.Forms.TextBox();
            this.lblkhoa = new System.Windows.Forms.Label();
            this.lblhoten = new System.Windows.Forms.Label();
            this.lblmasv = new System.Windows.Forms.Label();
            this.lblQuanli = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTimKiem)).BeginInit();
            this.SuspendLayout();
            // 
            // txtketquatim
            // 
            this.txtketquatim.Location = new System.Drawing.Point(662, 489);
            this.txtketquatim.Margin = new System.Windows.Forms.Padding(4);
            this.txtketquatim.Name = "txtketquatim";
            this.txtketquatim.Size = new System.Drawing.Size(125, 22);
            this.txtketquatim.TabIndex = 59;
            this.txtketquatim.TextChanged += new System.EventHandler(this.txtketquatim_TextChanged);
            // 
            // lblketquatim
            // 
            this.lblketquatim.AutoSize = true;
            this.lblketquatim.Location = new System.Drawing.Point(516, 489);
            this.lblketquatim.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblketquatim.Name = "lblketquatim";
            this.lblketquatim.Size = new System.Drawing.Size(113, 16);
            this.lblketquatim.TabIndex = 58;
            this.lblketquatim.Text = "Kết Quả Tìm Kiếm";
            this.lblketquatim.Click += new System.EventHandler(this.lblketquatim_Click);
            // 
            // btntrove
            // 
            this.btntrove.Location = new System.Drawing.Point(519, 201);
            this.btntrove.Margin = new System.Windows.Forms.Padding(4);
            this.btntrove.Name = "btntrove";
            this.btntrove.Size = new System.Drawing.Size(100, 28);
            this.btntrove.TabIndex = 57;
            this.btntrove.Text = "Trở về";
            this.btntrove.UseVisualStyleBackColor = true;
            this.btntrove.Click += new System.EventHandler(this.btntrove_Click);
            // 
            // dgvTimKiem
            // 
            this.dgvTimKiem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTimKiem.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colMSSV,
            this.colHoTen,
            this.colKhoa,
            this.colDTB});
            this.dgvTimKiem.Location = new System.Drawing.Point(149, 307);
            this.dgvTimKiem.Margin = new System.Windows.Forms.Padding(4);
            this.dgvTimKiem.Name = "dgvTimKiem";
            this.dgvTimKiem.RowHeadersWidth = 51;
            this.dgvTimKiem.Size = new System.Drawing.Size(554, 165);
            this.dgvTimKiem.TabIndex = 56;
            this.dgvTimKiem.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTimKiem_CellContentClick);
            // 
            // colMSSV
            // 
            this.colMSSV.HeaderText = "MSSV";
            this.colMSSV.MinimumWidth = 6;
            this.colMSSV.Name = "colMSSV";
            this.colMSSV.Width = 125;
            // 
            // colHoTen
            // 
            this.colHoTen.HeaderText = "Họ Tên";
            this.colHoTen.MinimumWidth = 6;
            this.colHoTen.Name = "colHoTen";
            this.colHoTen.Width = 125;
            // 
            // colKhoa
            // 
            this.colKhoa.HeaderText = "Khoa";
            this.colKhoa.MinimumWidth = 6;
            this.colKhoa.Name = "colKhoa";
            this.colKhoa.Width = 125;
            // 
            // colDTB
            // 
            this.colDTB.HeaderText = "Điểm Trung Bình";
            this.colDTB.MinimumWidth = 6;
            this.colDTB.Name = "colDTB";
            this.colDTB.Width = 125;
            // 
            // btnXoa
            // 
            this.btnXoa.Location = new System.Drawing.Point(385, 201);
            this.btnXoa.Margin = new System.Windows.Forms.Padding(4);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(100, 28);
            this.btnXoa.TabIndex = 55;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.UseVisualStyleBackColor = true;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnTim
            // 
            this.btnTim.Location = new System.Drawing.Point(272, 201);
            this.btnTim.Margin = new System.Windows.Forms.Padding(4);
            this.btnTim.Name = "btnTim";
            this.btnTim.Size = new System.Drawing.Size(100, 28);
            this.btnTim.TabIndex = 54;
            this.btnTim.Text = "Tìm Kiếm";
            this.btnTim.UseVisualStyleBackColor = true;
            this.btnTim.Click += new System.EventHandler(this.btnTim_Click);
            // 
            // cmbFaculty
            // 
            this.cmbFaculty.FormattingEnabled = true;
            this.cmbFaculty.Location = new System.Drawing.Point(272, 141);
            this.cmbFaculty.Margin = new System.Windows.Forms.Padding(4);
            this.cmbFaculty.Name = "cmbFaculty";
            this.cmbFaculty.Size = new System.Drawing.Size(160, 24);
            this.cmbFaculty.TabIndex = 53;
            this.cmbFaculty.SelectedIndexChanged += new System.EventHandler(this.cmbFaculty_SelectedIndexChanged);
            // 
            // txtFullname
            // 
            this.txtFullname.Location = new System.Drawing.Point(272, 102);
            this.txtFullname.Margin = new System.Windows.Forms.Padding(4);
            this.txtFullname.Name = "txtFullname";
            this.txtFullname.Size = new System.Drawing.Size(275, 22);
            this.txtFullname.TabIndex = 52;
            this.txtFullname.TextChanged += new System.EventHandler(this.txtFullname_TextChanged);
            // 
            // txtStudentID
            // 
            this.txtStudentID.Location = new System.Drawing.Point(272, 65);
            this.txtStudentID.Margin = new System.Windows.Forms.Padding(4);
            this.txtStudentID.Name = "txtStudentID";
            this.txtStudentID.Size = new System.Drawing.Size(275, 22);
            this.txtStudentID.TabIndex = 51;
            this.txtStudentID.TextChanged += new System.EventHandler(this.txtStudentID_TextChanged);
            // 
            // lblkhoa
            // 
            this.lblkhoa.AutoSize = true;
            this.lblkhoa.Location = new System.Drawing.Point(128, 145);
            this.lblkhoa.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblkhoa.Name = "lblkhoa";
            this.lblkhoa.Size = new System.Drawing.Size(38, 16);
            this.lblkhoa.TabIndex = 50;
            this.lblkhoa.Text = "Khoa";
            this.lblkhoa.Click += new System.EventHandler(this.lblkhoa_Click);
            // 
            // lblhoten
            // 
            this.lblhoten.AutoSize = true;
            this.lblhoten.Location = new System.Drawing.Point(128, 102);
            this.lblhoten.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblhoten.Name = "lblhoten";
            this.lblhoten.Size = new System.Drawing.Size(52, 16);
            this.lblhoten.TabIndex = 49;
            this.lblhoten.Text = "Họ Tên";
            this.lblhoten.Click += new System.EventHandler(this.lblhoten_Click);
            // 
            // lblmasv
            // 
            this.lblmasv.AutoSize = true;
            this.lblmasv.Location = new System.Drawing.Point(128, 65);
            this.lblmasv.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblmasv.Name = "lblmasv";
            this.lblmasv.Size = new System.Drawing.Size(85, 16);
            this.lblmasv.TabIndex = 48;
            this.lblmasv.Text = "Mã Sinh Viên";
            this.lblmasv.Click += new System.EventHandler(this.lblmasv_Click);
            // 
            // lblQuanli
            // 
            this.lblQuanli.AutoSize = true;
            this.lblQuanli.Location = new System.Drawing.Point(125, 15);
            this.lblQuanli.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblQuanli.Name = "lblQuanli";
            this.lblQuanli.Size = new System.Drawing.Size(127, 16);
            this.lblQuanli.TabIndex = 47;
            this.lblQuanli.Text = "Thông Tin Tìm Kiếm";
            this.lblQuanli.Click += new System.EventHandler(this.lblQuanli_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(971, 582);
            this.Controls.Add(this.txtketquatim);
            this.Controls.Add(this.lblketquatim);
            this.Controls.Add(this.btntrove);
            this.Controls.Add(this.dgvTimKiem);
            this.Controls.Add(this.btnXoa);
            this.Controls.Add(this.btnTim);
            this.Controls.Add(this.cmbFaculty);
            this.Controls.Add(this.txtFullname);
            this.Controls.Add(this.txtStudentID);
            this.Controls.Add(this.lblkhoa);
            this.Controls.Add(this.lblhoten);
            this.Controls.Add(this.lblmasv);
            this.Controls.Add(this.lblQuanli);
            this.Name = "Form3";
            this.Text = "Form3";
            ((System.ComponentModel.ISupportInitialize)(this.dgvTimKiem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtketquatim;
        private System.Windows.Forms.Label lblketquatim;
        private System.Windows.Forms.Button btntrove;
        private System.Windows.Forms.DataGridView dgvTimKiem;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMSSV;
        private System.Windows.Forms.DataGridViewTextBoxColumn colHoTen;
        private System.Windows.Forms.DataGridViewTextBoxColumn colKhoa;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDTB;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.Button btnTim;
        private System.Windows.Forms.ComboBox cmbFaculty;
        private System.Windows.Forms.TextBox txtFullname;
        private System.Windows.Forms.TextBox txtStudentID;
        private System.Windows.Forms.Label lblkhoa;
        private System.Windows.Forms.Label lblhoten;
        private System.Windows.Forms.Label lblmasv;
        private System.Windows.Forms.Label lblQuanli;
    }
}