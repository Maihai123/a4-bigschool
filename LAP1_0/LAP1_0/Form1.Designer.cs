﻿namespace LAP1_0
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.btnthoat = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbFaculty = new System.Windows.Forms.ComboBox();
            this.txtAverageSocre = new System.Windows.Forms.TextBox();
            this.txtFullname = new System.Windows.Forms.TextBox();
            this.txtStudentID = new System.Windows.Forms.TextBox();
            this.lblkhoa = new System.Windows.Forms.Label();
            this.lbldiemtb = new System.Windows.Forms.Label();
            this.lblhoten = new System.Windows.Forms.Label();
            this.lblmasv = new System.Windows.Forms.Label();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripbtnQLK = new System.Windows.Forms.ToolStripButton();
            this.toolStripbtntim = new System.Windows.Forms.ToolStripButton();
            this.dgvStudent = new System.Windows.Forms.DataGridView();
            this.colMSSV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colHoTen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colKhoa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colDTB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpDate = new System.Windows.Forms.Button();
            this.lblQuanli = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudent)).BeginInit();
            this.SuspendLayout();
            // 
            // btnthoat
            // 
            this.btnthoat.Location = new System.Drawing.Point(382, 348);
            this.btnthoat.Margin = new System.Windows.Forms.Padding(4);
            this.btnthoat.Name = "btnthoat";
            this.btnthoat.Size = new System.Drawing.Size(100, 28);
            this.btnthoat.TabIndex = 55;
            this.btnthoat.Text = "Thoát";
            this.btnthoat.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbFaculty);
            this.groupBox1.Controls.Add(this.txtAverageSocre);
            this.groupBox1.Controls.Add(this.txtFullname);
            this.groupBox1.Controls.Add(this.txtStudentID);
            this.groupBox1.Controls.Add(this.lblkhoa);
            this.groupBox1.Controls.Add(this.lbldiemtb);
            this.groupBox1.Controls.Add(this.lblhoten);
            this.groupBox1.Controls.Add(this.lblmasv);
            this.groupBox1.Location = new System.Drawing.Point(53, 70);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(524, 217);
            this.groupBox1.TabIndex = 54;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông Tin Sinh Viên";
            // 
            // cmbFaculty
            // 
            this.cmbFaculty.FormattingEnabled = true;
            this.cmbFaculty.Items.AddRange(new object[] {
            "QTKD",
            "CNTT",
            "NNA"});
            this.cmbFaculty.Location = new System.Drawing.Point(154, 132);
            this.cmbFaculty.Margin = new System.Windows.Forms.Padding(4);
            this.cmbFaculty.Name = "cmbFaculty";
            this.cmbFaculty.Size = new System.Drawing.Size(160, 24);
            this.cmbFaculty.TabIndex = 42;
            this.cmbFaculty.Text = "CNTT";
            // 
            // txtAverageSocre
            // 
            this.txtAverageSocre.Location = new System.Drawing.Point(154, 97);
            this.txtAverageSocre.Margin = new System.Windows.Forms.Padding(4);
            this.txtAverageSocre.Name = "txtAverageSocre";
            this.txtAverageSocre.Size = new System.Drawing.Size(91, 22);
            this.txtAverageSocre.TabIndex = 41;
            // 
            // txtFullname
            // 
            this.txtFullname.Location = new System.Drawing.Point(154, 50);
            this.txtFullname.Margin = new System.Windows.Forms.Padding(4);
            this.txtFullname.Name = "txtFullname";
            this.txtFullname.Size = new System.Drawing.Size(275, 22);
            this.txtFullname.TabIndex = 40;
            // 
            // txtStudentID
            // 
            this.txtStudentID.Location = new System.Drawing.Point(154, 13);
            this.txtStudentID.Margin = new System.Windows.Forms.Padding(4);
            this.txtStudentID.Name = "txtStudentID";
            this.txtStudentID.Size = new System.Drawing.Size(275, 22);
            this.txtStudentID.TabIndex = 39;
            // 
            // lblkhoa
            // 
            this.lblkhoa.AutoSize = true;
            this.lblkhoa.Location = new System.Drawing.Point(10, 144);
            this.lblkhoa.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblkhoa.Name = "lblkhoa";
            this.lblkhoa.Size = new System.Drawing.Size(38, 16);
            this.lblkhoa.TabIndex = 38;
            this.lblkhoa.Text = "Khoa";
            // 
            // lbldiemtb
            // 
            this.lbldiemtb.AutoSize = true;
            this.lbldiemtb.Location = new System.Drawing.Point(10, 100);
            this.lbldiemtb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbldiemtb.Name = "lbldiemtb";
            this.lbldiemtb.Size = new System.Drawing.Size(58, 16);
            this.lbldiemtb.TabIndex = 37;
            this.lbldiemtb.Text = "Điểm Tb";
            // 
            // lblhoten
            // 
            this.lblhoten.AutoSize = true;
            this.lblhoten.Location = new System.Drawing.Point(10, 50);
            this.lblhoten.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblhoten.Name = "lblhoten";
            this.lblhoten.Size = new System.Drawing.Size(52, 16);
            this.lblhoten.TabIndex = 36;
            this.lblhoten.Text = "Họ Tên";
            // 
            // lblmasv
            // 
            this.lblmasv.AutoSize = true;
            this.lblmasv.Location = new System.Drawing.Point(10, 19);
            this.lblmasv.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblmasv.Name = "lblmasv";
            this.lblmasv.Size = new System.Drawing.Size(85, 16);
            this.lblmasv.TabIndex = 35;
            this.lblmasv.Text = "Mã Sinh Viên";
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripbtnQLK,
            this.toolStripbtntim});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1204, 27);
            this.toolStrip1.TabIndex = 53;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripbtnQLK
            // 
            this.toolStripbtnQLK.Image = ((System.Drawing.Image)(resources.GetObject("toolStripbtnQLK.Image")));
            this.toolStripbtnQLK.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripbtnQLK.Name = "toolStripbtnQLK";
            this.toolStripbtnQLK.Size = new System.Drawing.Size(121, 24);
            this.toolStripbtnQLK.Text = "Quản Lí Khoa";
            // 
            // toolStripbtntim
            // 
            this.toolStripbtntim.Image = ((System.Drawing.Image)(resources.GetObject("toolStripbtntim.Image")));
            this.toolStripbtntim.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripbtntim.Name = "toolStripbtntim";
            this.toolStripbtntim.Size = new System.Drawing.Size(96, 24);
            this.toolStripbtntim.Text = "Tìm Kiếm";
            // 
            // dgvStudent
            // 
            this.dgvStudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStudent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colMSSV,
            this.colHoTen,
            this.colKhoa,
            this.colDTB});
            this.dgvStudent.Location = new System.Drawing.Point(584, 70);
            this.dgvStudent.Margin = new System.Windows.Forms.Padding(4);
            this.dgvStudent.Name = "dgvStudent";
            this.dgvStudent.RowHeadersWidth = 51;
            this.dgvStudent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvStudent.Size = new System.Drawing.Size(554, 362);
            this.dgvStudent.TabIndex = 52;
            // 
            // colMSSV
            // 
            this.colMSSV.HeaderText = "MSSV";
            this.colMSSV.MinimumWidth = 6;
            this.colMSSV.Name = "colMSSV";
            this.colMSSV.Width = 125;
            // 
            // colHoTen
            // 
            this.colHoTen.HeaderText = "Họ Tên";
            this.colHoTen.MinimumWidth = 6;
            this.colHoTen.Name = "colHoTen";
            this.colHoTen.Width = 125;
            // 
            // colKhoa
            // 
            this.colKhoa.HeaderText = "Khoa";
            this.colKhoa.MinimumWidth = 6;
            this.colKhoa.Name = "colKhoa";
            this.colKhoa.Width = 125;
            // 
            // colDTB
            // 
            this.colDTB.HeaderText = "Điểm Trung Bình";
            this.colDTB.MinimumWidth = 6;
            this.colDTB.Name = "colDTB";
            this.colDTB.Width = 125;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(225, 348);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 28);
            this.btnDelete.TabIndex = 51;
            this.btnDelete.Text = "Xoá";
            this.btnDelete.UseVisualStyleBackColor = true;
            // 
            // btnUpDate
            // 
            this.btnUpDate.Location = new System.Drawing.Point(66, 348);
            this.btnUpDate.Margin = new System.Windows.Forms.Padding(4);
            this.btnUpDate.Name = "btnUpDate";
            this.btnUpDate.Size = new System.Drawing.Size(100, 28);
            this.btnUpDate.TabIndex = 50;
            this.btnUpDate.Text = "Thêm / Sửa";
            this.btnUpDate.UseVisualStyleBackColor = true;
  
            // 
            // lblQuanli
            // 
            this.lblQuanli.AutoSize = true;
            this.lblQuanli.Location = new System.Drawing.Point(492, 30);
            this.lblQuanli.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblQuanli.Name = "lblQuanli";
            this.lblQuanli.Size = new System.Drawing.Size(175, 16);
            this.lblQuanli.TabIndex = 49;
            this.lblQuanli.Text = "Quản Lí Thông Tin Sinh Viên";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1204, 538);
            this.Controls.Add(this.btnthoat);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.dgvStudent);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpDate);
            this.Controls.Add(this.lblQuanli);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnthoat;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cmbFaculty;
        private System.Windows.Forms.TextBox txtAverageSocre;
        private System.Windows.Forms.TextBox txtFullname;
        private System.Windows.Forms.TextBox txtStudentID;
        private System.Windows.Forms.Label lblkhoa;
        private System.Windows.Forms.Label lbldiemtb;
        private System.Windows.Forms.Label lblhoten;
        private System.Windows.Forms.Label lblmasv;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripbtnQLK;
        private System.Windows.Forms.ToolStripButton toolStripbtntim;
        private System.Windows.Forms.DataGridView dgvStudent;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMSSV;
        private System.Windows.Forms.DataGridViewTextBoxColumn colHoTen;
        private System.Windows.Forms.DataGridViewTextBoxColumn colKhoa;
        private System.Windows.Forms.DataGridViewTextBoxColumn colDTB;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpDate;
        private System.Windows.Forms.Label lblQuanli;
    }
}

