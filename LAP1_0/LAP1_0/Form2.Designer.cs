﻿namespace LAP1_0
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btndong = new System.Windows.Forms.Button();
            this.dgvFaculty = new System.Windows.Forms.DataGridView();
            this.colMK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTenKhoa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTONGGS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpDate = new System.Windows.Forms.Button();
            this.txtTotalProfessor = new System.Windows.Forms.TextBox();
            this.txtFacultyName = new System.Windows.Forms.TextBox();
            this.txtFacultyID = new System.Windows.Forms.TextBox();
            this.lbltong = new System.Windows.Forms.Label();
            this.lbltenkhoa = new System.Windows.Forms.Label();
            this.lblmakhoa = new System.Windows.Forms.Label();
            this.lblthongtinkhoa = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFaculty)).BeginInit();
            this.SuspendLayout();
            // 
            // btndong
            // 
            this.btndong.Location = new System.Drawing.Point(524, 299);
            this.btndong.Margin = new System.Windows.Forms.Padding(4);
            this.btndong.Name = "btndong";
            this.btndong.Size = new System.Drawing.Size(100, 28);
            this.btndong.TabIndex = 62;
            this.btndong.Text = "Đóng";
            this.btndong.UseVisualStyleBackColor = true;
            // 
            // dgvFaculty
            // 
            this.dgvFaculty.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFaculty.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colMK,
            this.colTenKhoa,
            this.colTONGGS});
            this.dgvFaculty.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dgvFaculty.Location = new System.Drawing.Point(682, 74);
            this.dgvFaculty.Margin = new System.Windows.Forms.Padding(4);
            this.dgvFaculty.Name = "dgvFaculty";
            this.dgvFaculty.RowHeadersWidth = 51;
            this.dgvFaculty.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvFaculty.Size = new System.Drawing.Size(459, 362);
            this.dgvFaculty.TabIndex = 61;
            // 
            // colMK
            // 
            this.colMK.HeaderText = "Mã Khoa";
            this.colMK.MinimumWidth = 6;
            this.colMK.Name = "colMK";
            this.colMK.Width = 125;
            // 
            // colTenKhoa
            // 
            this.colTenKhoa.HeaderText = "Tên Khoa";
            this.colTenKhoa.MinimumWidth = 6;
            this.colTenKhoa.Name = "colTenKhoa";
            this.colTenKhoa.Width = 125;
            // 
            // colTONGGS
            // 
            this.colTONGGS.HeaderText = "Tổng Số GS";
            this.colTONGGS.MinimumWidth = 6;
            this.colTONGGS.Name = "colTONGGS";
            this.colTONGGS.Width = 125;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(397, 299);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 28);
            this.btnDelete.TabIndex = 60;
            this.btnDelete.Text = "Xoá";
            this.btnDelete.UseVisualStyleBackColor = true;
            // 
            // btnUpDate
            // 
            this.btnUpDate.Location = new System.Drawing.Point(284, 299);
            this.btnUpDate.Margin = new System.Windows.Forms.Padding(4);
            this.btnUpDate.Name = "btnUpDate";
            this.btnUpDate.Size = new System.Drawing.Size(100, 28);
            this.btnUpDate.TabIndex = 59;
            this.btnUpDate.Text = "Thêm / Sửa";
            this.btnUpDate.UseVisualStyleBackColor = true;
            // 
            // txtTotalProfessor
            // 
            this.txtTotalProfessor.Location = new System.Drawing.Point(284, 195);
            this.txtTotalProfessor.Margin = new System.Windows.Forms.Padding(4);
            this.txtTotalProfessor.Name = "txtTotalProfessor";
            this.txtTotalProfessor.Size = new System.Drawing.Size(91, 22);
            this.txtTotalProfessor.TabIndex = 58;
            // 
            // txtFacultyName
            // 
            this.txtFacultyName.Location = new System.Drawing.Point(284, 149);
            this.txtFacultyName.Margin = new System.Windows.Forms.Padding(4);
            this.txtFacultyName.Name = "txtFacultyName";
            this.txtFacultyName.Size = new System.Drawing.Size(275, 22);
            this.txtFacultyName.TabIndex = 57;
            // 
            // txtFacultyID
            // 
            this.txtFacultyID.Location = new System.Drawing.Point(284, 112);
            this.txtFacultyID.Margin = new System.Windows.Forms.Padding(4);
            this.txtFacultyID.Name = "txtFacultyID";
            this.txtFacultyID.Size = new System.Drawing.Size(275, 22);
            this.txtFacultyID.TabIndex = 56;
            this.txtFacultyID.TextChanged += new System.EventHandler(this.txtFacultyID_TextChanged);
            // 
            // lbltong
            // 
            this.lbltong.AutoSize = true;
            this.lbltong.Location = new System.Drawing.Point(140, 199);
            this.lbltong.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbltong.Name = "lbltong";
            this.lbltong.Size = new System.Drawing.Size(81, 16);
            this.lbltong.TabIndex = 55;
            this.lbltong.Text = "Tổng Số GS";
            // 
            // lbltenkhoa
            // 
            this.lbltenkhoa.AutoSize = true;
            this.lbltenkhoa.Location = new System.Drawing.Point(140, 149);
            this.lbltenkhoa.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbltenkhoa.Name = "lbltenkhoa";
            this.lbltenkhoa.Size = new System.Drawing.Size(65, 16);
            this.lbltenkhoa.TabIndex = 54;
            this.lbltenkhoa.Text = "Tên Khoa";
            // 
            // lblmakhoa
            // 
            this.lblmakhoa.AutoSize = true;
            this.lblmakhoa.Location = new System.Drawing.Point(140, 112);
            this.lblmakhoa.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblmakhoa.Name = "lblmakhoa";
            this.lblmakhoa.Size = new System.Drawing.Size(60, 16);
            this.lblmakhoa.TabIndex = 53;
            this.lblmakhoa.Text = "Mã Khoa";
            // 
            // lblthongtinkhoa
            // 
            this.lblthongtinkhoa.AutoSize = true;
            this.lblthongtinkhoa.Location = new System.Drawing.Point(140, 23);
            this.lblthongtinkhoa.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblthongtinkhoa.Name = "lblthongtinkhoa";
            this.lblthongtinkhoa.Size = new System.Drawing.Size(102, 16);
            this.lblthongtinkhoa.TabIndex = 52;
            this.lblthongtinkhoa.Text = "Thông Tin Khoa";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1275, 612);
            this.Controls.Add(this.btndong);
            this.Controls.Add(this.dgvFaculty);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpDate);
            this.Controls.Add(this.txtTotalProfessor);
            this.Controls.Add(this.txtFacultyName);
            this.Controls.Add(this.txtFacultyID);
            this.Controls.Add(this.lbltong);
            this.Controls.Add(this.lbltenkhoa);
            this.Controls.Add(this.lblmakhoa);
            this.Controls.Add(this.lblthongtinkhoa);
            this.Name = "Form2";
            this.Text = "Form2";
            ((System.ComponentModel.ISupportInitialize)(this.dgvFaculty)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btndong;
        private System.Windows.Forms.DataGridView dgvFaculty;
        private System.Windows.Forms.DataGridViewTextBoxColumn colMK;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTenKhoa;
        private System.Windows.Forms.DataGridViewTextBoxColumn colTONGGS;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpDate;
        private System.Windows.Forms.TextBox txtTotalProfessor;
        private System.Windows.Forms.TextBox txtFacultyName;
        private System.Windows.Forms.TextBox txtFacultyID;
        private System.Windows.Forms.Label lbltong;
        private System.Windows.Forms.Label lbltenkhoa;
        private System.Windows.Forms.Label lblmakhoa;
        private System.Windows.Forms.Label lblthongtinkhoa;
    }
}