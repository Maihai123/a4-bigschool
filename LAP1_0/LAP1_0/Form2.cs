﻿
using LAP1_0.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LAP1_0
{
    public partial class Form2 : Form
    {
        StudentContextDB db;
        public Form2()
        {
            InitializeComponent();
            db = new StudentContextDB();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            LoadData();
            dgvFaculty.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

        }

        private void LoadData()
        {
            List<Faculty> facultyList = db.Faculty.ToList();
            bindingData(facultyList);
        }

        private void bindingData(List<Faculty> facLst)
        {
            dgvFaculty.Rows.Clear();
            foreach (Faculty item in facLst)
            {
                int index = dgvFaculty.Rows.Add();
                dgvFaculty.Rows[index].Cells[0].Value = item.FacultyID;
                dgvFaculty.Rows[index].Cells[1].Value = item.FacultyName;
                dgvFaculty.Rows[index].Cells[2].Value = item.TotalProfessor;
            }
        }

        private void btnUpDate_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtFacultyID.Text))
                    throw new Exception("Vui lòng nhập mã Khoa!");

                int facultyID;
                if (!int.TryParse(txtFacultyID.Text.Trim(), out facultyID))
                {
                    throw new Exception("Mã Khoa không hợp lệ. Vui lòng nhập lại!");
                }

                string facultyName = txtFacultyName.Text.Trim();

                // Kiểm tra nếu tên khoa trống
                if (string.IsNullOrEmpty(facultyName))
                {
                    throw new Exception("Vui lòng nhập tên Khoa!");
                }

                int totalProfessor; // Thêm biến totalProfessor
                if (!int.TryParse(txtTotalProfessor.Text.Trim(), out totalProfessor))
                {
                    throw new Exception("Tổng số Giáo Sư không hợp lệ. Vui lòng nhập lại!");
                }

                Faculty existingFaculty = db.Faculty.FirstOrDefault(f => f.FacultyID == facultyID);

                if (existingFaculty == null)
                {
                    // Khoa chưa tồn tại, thêm mới
                    Faculty newFaculty = new Faculty()
                    {
                        FacultyID = facultyID, // Gán facultyID kiểu int
                        FacultyName = facultyName,
                        TotalProfessor = totalProfessor // Gán TotalProfessor
                    };
                    db.Faculty.Add(newFaculty);
                    db.SaveChanges();
                    LoadData();
                    MessageBox.Show("Thêm mới Khoa thành công!", "Thông Báo", MessageBoxButtons.OK);
                    txtFacultyID.Clear();
                    txtFacultyName.Clear();
                    txtTotalProfessor.Clear();
                }
                else
                {
                    // Khoa đã tồn tại, cập nhật dữ liệu
                    existingFaculty.FacultyName = facultyName;
                    existingFaculty.TotalProfessor = totalProfessor; // Cập nhật TotalProfessor
                    db.SaveChanges();
                    LoadData();
                    MessageBox.Show("Cập nhật Khoa thành công!", "Thông Báo", MessageBoxButtons.OK);
                    txtFacultyID.Clear();
                    txtFacultyName.Clear();
                    txtTotalProfessor.Clear();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtFacultyID.Text))
                {
                    MessageBox.Show("Vui lòng nhập mã Khoa cần xóa!", "Thông Báo", MessageBoxButtons.OK);
                    return; // Thoát khỏi phương thức mà không thực hiện thêm xử lý
                }

                if (dgvFaculty.SelectedRows.Count > 0)
                {
                    int selectedRowIndex = dgvFaculty.SelectedRows[0].Index;
                    int facultyID = (int)dgvFaculty.Rows[selectedRowIndex].Cells[0].Value;

                    var facultyToDelete = db.Faculty.FirstOrDefault(f => f.FacultyID == facultyID);

                    if (facultyToDelete != null)
                    {
                        db.Faculty.Remove(facultyToDelete);
                        db.SaveChanges();
                        LoadData();
                        txtFacultyName.Clear();
                        MessageBox.Show("Xóa mã Khoa thành công!", "Thông Báo", MessageBoxButtons.OK);
                    }
                }
                else
                {
                    throw new Exception("Vui lòng chọn mã Khoa cần xóa!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void dgvFaculty_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                DataGridViewRow selectedRow = dgvFaculty.Rows[e.RowIndex];

                // Truy cập dữ liệu từ hàng hiện tại đang được nhấp vào
                string facultyID = selectedRow.Cells[0].Value.ToString();
                string facultyName = selectedRow.Cells[1].Value.ToString();
                string totalProfessor = selectedRow.Cells[2].Value.ToString();
                txtFacultyID.Text = facultyID;
                txtFacultyName.Text = facultyName;
                txtTotalProfessor.Text = totalProfessor;
            }
        }

        private void btndong_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
