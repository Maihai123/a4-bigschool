﻿

using LAP1_0.Models;
using LAP1_0;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LAP1_0
{
    public partial class Form1 : Form
    {
        StudentContextDB db;
        public Form1()
        {
            InitializeComponent();
            db = new StudentContextDB();

        }



        // Phương thức để lấy danh sách tên khoa từ dgvSinhVien
        private List<string> GetFacultyNamesFromDataGridView()
        {
            List<string> facultyNames = new List<string>();

            foreach (DataGridViewRow row in dgvStudent.Rows)
            {
                if (row.Cells[2].Value != null)
                {
                    string facultyName = row.Cells[2].Value.ToString();
                    if (!string.IsNullOrWhiteSpace(facultyName) && !facultyNames.Contains(facultyName))
                    {
                        facultyNames.Add(facultyName);
                    }
                }
            }

            return facultyNames;
        }
        private void btnthoat_Click(object sender, EventArgs e)
        {
            {
                DialogResult result = MessageBox.Show("Bạn có muốn thoát chương trình không?", "Xác nhận thoát", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    Application.Exit(); // Thoát chương trình nếu người dùng chọn "Yes"
                }
            }
        }

        private void bindingData(List<Student> stuLst)
        {
            dgvStudent.Rows.Clear();
            foreach (Student item in stuLst)
            {
                int index = dgvStudent.Rows.Add();
                dgvStudent.Rows[index].Cells[0].Value = item.StudentID;
                dgvStudent.Rows[index].Cells[1].Value = item.FullName;
                dgvStudent.Rows[index].Cells[2].Value = item.Faculty.FacultyName;
                dgvStudent.Rows[index].Cells[3].Value = item.AverageScore;
            }
        }
        private void bindingCombobox(List<Faculty> facLst)
        {
            cmbFaculty.DataSource = facLst;
            cmbFaculty.DisplayMember = "FacultyName";
            cmbFaculty.ValueMember = "FacultyID";
        }
        private int GetSelectedRow(string StudentID)
        {
            for (int i = 0; i < dgvStudent.Rows.Count; i++)
            {
                if (dgvStudent.Rows[i].Cells[0].Value != null)
                {
                    if (dgvStudent.Rows[i].Cells[0].Value.ToString() == StudentID)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }
        private void InsertUpdate(int SelectedRow)
        {
            dgvStudent.Rows[SelectedRow].Cells[0].Value = txtStudentID.Text;
            dgvStudent.Rows[SelectedRow].Cells[1].Value = txtFullname.Text;
            dgvStudent.Rows[SelectedRow].Cells[2].Value = float.Parse(txtAverageSocre.Text).ToString();
            dgvStudent.Rows[SelectedRow].Cells[3].Value = cmbFaculty.Text;
        }

        private void refreshs()
        {
            txtStudentID.Text = "";
            txtFullname.Text = "";
            txtAverageSocre.Text = "";
            cmbFaculty.SelectedIndex = 0;
        }


        public void ReloadData()
        {
            List<Student> stu = db.Students.ToList();
            bindingData(stu);
        }
        private void LoadData()
        {
            List<Student> stu = db.Students.ToList();
            bindingData(stu);
        }
        private bool ContainsNumericCharacters(string text)
        {
            return text.Any(char.IsDigit);
        }


        private void btnUpDate_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (txtStudentID.Text == "" || txtFullname.Text == "" || txtAverageSocre.Text == "")
                    throw new Exception("Vui lòng nhập đầy đủ thông tin sinh viên !");
                // Kiểm tra độ dài của họ tên
                if (txtFullname.Text.Length < 3 || txtFullname.Text.Length > 100)
                    throw new Exception("Họ tên không hợp lệ. Vui lòng nhập lại!");
                if (ContainsNumericCharacters(txtFullname.Text))
                    throw new Exception("Họ tên không được chứa ký tự số. Vui lòng nhập lại!");
                // Kiểm tra độ dài của mã số sinh viên
                if (txtStudentID.Text.Length != 10)
                    throw new Exception("Mã số sinh viên không hợp lệ. Vui lòng nhập lại!");
                // Kiểm tra mã số sinh viên chỉ chứa các ký tự số
                int studentID;
                if (!int.TryParse(txtStudentID.Text, out studentID))
                    throw new Exception("Mã số sinh viên không hợp lệ. Vui lòng nhập lại!");

                // Kiểm tra điểm trung bình
                float averageScore;
                if (!float.TryParse(txtAverageSocre.Text, out averageScore) || averageScore < 0 || averageScore > 10)
                    throw new Exception("Điểm trung bình không hợp lệ. Vui lòng nhập lại!");


                string studentIDToUpdate = txtStudentID.Text;
                Student existingStudent = db.Students.FirstOrDefault(s => s.StudentID == studentIDToUpdate);

                if (existingStudent == null)
                {
                    // Mã số sinh viên chưa tồn tại, thực hiện thêm mới
                    Student newStudent = new Student()
                    {
                        StudentID = txtStudentID.Text,
                        FullName = txtFullname.Text,
                        FacultyID = int.Parse(cmbFaculty.SelectedValue.ToString()),
                        AverageScore = float.Parse(txtAverageSocre.Text)
                    };
                    db.Students.Add(newStudent);
                    db.SaveChanges();
                    LoadData();
                    MessageBox.Show("Thêm mới dữ liệu thành công!", "Thông Báo", MessageBoxButtons.OK);
                    refreshs();
                }
                else
                {
                    // Mã số sinh viên đã tồn tại, thực hiện cập nhật thông tin
                    existingStudent.FullName = txtFullname.Text;
                    existingStudent.FacultyID = int.Parse(cmbFaculty.SelectedValue.ToString());
                    existingStudent.AverageScore = float.Parse(txtAverageSocre.Text);
                    db.SaveChanges();
                    LoadData();
                    MessageBox.Show("Cập nhật dữ liệu thành công!", "Thông báo", MessageBoxButtons.OK);
                    refreshs();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDelete_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (txtStudentID.Text == "")
                    throw new Exception("Vui lòng nhập Mã số sinh viên cần xóa!");

                int selectedRow = GetSelectedRow(txtStudentID.Text);
                if (selectedRow == -1)
                {
                    throw new Exception("Không tìm thấy MSSV cần xóa!");
                }
                else
                {
                    DialogResult result = MessageBox.Show("Bạn có chắc chắn muốn xóa sinh viên này?", "Cảnh báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (result == DialogResult.Yes)
                    {
                        // Lấy thông tin sinh viên cần xóa
                        string studentIDToDelete = txtStudentID.Text;
                        Student studentToDelete = db.Students.FirstOrDefault(s => s.StudentID == studentIDToDelete);

                        if (studentToDelete != null)
                        {
                            // Xóa sinh viên khỏi cơ sở dữ liệu
                            db.Students.Remove(studentToDelete);
                            db.SaveChanges(); // Lưu thay đổi vào cơ sở dữ liệu

                            // Xóa dòng trong DataGridView
                            dgvStudent.Rows.RemoveAt(selectedRow);

                            MessageBox.Show("Xóa sinh viên thành công!", "Thông báo", MessageBoxButtons.OK);
                        }
                        else
                        {
                            throw new Exception("Không tìm thấy sinh viên để xóa!");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void toolStripbtnQLK_Click_1(object sender, EventArgs e)
        {

            Form2 form2 = new Form2();

            form2.ShowDialog();
        }

        private void ToolStripbtntim_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3(GetFacultyNamesFromDataGridView());
            form3.ShowDialog();
        }

        private void dgvStudent_CellContentClick_1(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex != -1)
            {
                txtStudentID.Text = dgvStudent.Rows[e.RowIndex].Cells[0].Value.ToString();
                txtFullname.Text = dgvStudent.Rows[e.RowIndex].Cells[1].Value.ToString();
                cmbFaculty.SelectedIndex = cmbFaculty.FindString(dgvStudent.Rows[e.RowIndex].Cells[2].Value.ToString());
                txtAverageSocre.Text = dgvStudent.Rows[e.RowIndex].Cells[3].Value.ToString();

            }
        }

        private void Form1_Load_1(object sender, EventArgs e)
        {

            List<Student> studentList = db.Students.ToList();
            List<Faculty> facultyList = db.Faculty.ToList();
            bindingData(studentList);
            bindingCombobox(facultyList);
        }
    }
}


