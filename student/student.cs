﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace student
{
    internal class student
    {
        public String MSSV_ID;
        public String NAME;
        public float  DTB;
        public String KHOA;

        public void input()
        {
            Console.WriteLine(" NHAP ID  STUDENT ");
            this.MSSV_ID = Console.ReadLine();
            Console.WriteLine(" NHAP NAME  STUDENT ");
            this.NAME = Console.ReadLine();
            Console.WriteLine(" NHAP DTB  STUDENT ");
            this.DTB = float.Parse(Console.ReadLine());
            Console.WriteLine(" NHAP KHOA  STUDENT ");
            this.KHOA = Console.ReadLine();
        }

        public void output()
        {
            Console.WriteLine(" ID  STUDENT " +this.MSSV_ID);
            Console.WriteLine(" NAME  STUDENT "+ this.NAME);
            Console.WriteLine("DTB STUDENT "+this.DTB);
            Console.WriteLine(" KHOA STUDENT " + this.KHOA);
        }
    }
}
