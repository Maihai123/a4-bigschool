﻿using BAILAM.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BAILAM
{
   
    public partial class Form1 : Form
    {
      
            StudentContextDB db;
            public Form1()
            {
                InitializeComponent();
                db = new StudentContextDB();

            }
          
        #region methods
        void AddSinhVien()
        {

        }
        void DeleteSinhVien()
        {

        }
        void EditSinhVien()
        {

        }
        void XemSinhVien()
        {
            dtgvSinhVien.DataSource = db.Student.ToList();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddSinhVien();
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            DeleteSinhVien();
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            EditSinhVien();
        }

        private void btnXem_Click(object sender, EventArgs e)
        {
            XemSinhVien();
        }
    }
    #endregion
}