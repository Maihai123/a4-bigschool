﻿namespace BAI1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tetTenNV = new System.Windows.Forms.TextBox();
            this.tetLuongCB = new System.Windows.Forms.TextBox();
            this.btnDongY = new System.Windows.Forms.Button();
            this.txtMaNV = new System.Windows.Forms.TextBox();
            this.btnMNV = new System.Windows.Forms.Button();
            this.btnTenNV = new System.Windows.Forms.Button();
            this.btnLuongNV = new System.Windows.Forms.Button();
            this.btnThoat = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // tetTenNV
            // 
            this.tetTenNV.Location = new System.Drawing.Point(459, 187);
            this.tetTenNV.Name = "tetTenNV";
            this.tetTenNV.Size = new System.Drawing.Size(100, 22);
            this.tetTenNV.TabIndex = 2;
            // 
            // tetLuongCB
            // 
            this.tetLuongCB.Location = new System.Drawing.Point(459, 253);
            this.tetLuongCB.Name = "tetLuongCB";
            this.tetLuongCB.Size = new System.Drawing.Size(100, 22);
            this.tetLuongCB.TabIndex = 3;
            // 
            // btnDongY
            // 
            this.btnDongY.Location = new System.Drawing.Point(209, 311);
            this.btnDongY.Name = "btnDongY";
            this.btnDongY.Size = new System.Drawing.Size(75, 23);
            this.btnDongY.TabIndex = 4;
            this.btnDongY.Text = "Dongy";
            this.btnDongY.UseVisualStyleBackColor = true;
            // 
            // txtMaNV
            // 
            this.txtMaNV.Location = new System.Drawing.Point(459, 133);
            this.txtMaNV.Name = "txtMaNV";
            this.txtMaNV.Size = new System.Drawing.Size(100, 22);
            this.txtMaNV.TabIndex = 0;
            this.txtMaNV.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // btnMNV
            // 
            this.btnMNV.Location = new System.Drawing.Point(318, 133);
            this.btnMNV.Name = "btnMNV";
            this.btnMNV.Size = new System.Drawing.Size(75, 23);
            this.btnMNV.TabIndex = 5;
            this.btnMNV.Text = "MSNV";
            this.btnMNV.UseVisualStyleBackColor = true;
            // 
            // btnTenNV
            // 
            this.btnTenNV.Location = new System.Drawing.Point(318, 187);
            this.btnTenNV.Name = "btnTenNV";
            this.btnTenNV.Size = new System.Drawing.Size(75, 23);
            this.btnTenNV.TabIndex = 6;
            this.btnTenNV.Text = "TenNV";
            this.btnTenNV.UseVisualStyleBackColor = true;
            // 
            // btnLuongNV
            // 
            this.btnLuongNV.Location = new System.Drawing.Point(318, 253);
            this.btnLuongNV.Name = "btnLuongNV";
            this.btnLuongNV.Size = new System.Drawing.Size(75, 23);
            this.btnLuongNV.TabIndex = 7;
            this.btnLuongNV.Text = "LuongNV";
            this.btnLuongNV.UseVisualStyleBackColor = true;
            this.btnLuongNV.Click += new System.EventHandler(this.button4_Click);
            // 
            // btnThoat
            // 
            this.btnThoat.Location = new System.Drawing.Point(365, 311);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(75, 23);
            this.btnThoat.TabIndex = 8;
            this.btnThoat.Text = "thoat";
            this.btnThoat.UseVisualStyleBackColor = true;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnThoat);
            this.Controls.Add(this.btnLuongNV);
            this.Controls.Add(this.btnTenNV);
            this.Controls.Add(this.btnMNV);
            this.Controls.Add(this.btnDongY);
            this.Controls.Add(this.tetLuongCB);
            this.Controls.Add(this.tetTenNV);
            this.Controls.Add(this.txtMaNV);
            this.Name = "Form2";
            this.Text = "NHANVIEN\'";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.TextBox tetTenNV;
        private System.Windows.Forms.TextBox tetLuongCB;
        private System.Windows.Forms.Button btnDongY;
        private System.Windows.Forms.TextBox txtMaNV;
        private System.Windows.Forms.Button btnMNV;
        private System.Windows.Forms.Button btnTenNV;
        private System.Windows.Forms.Button btnLuongNV;
        private System.Windows.Forms.Button btnThoat;
    }
}