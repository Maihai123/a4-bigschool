﻿namespace Lab05.GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picAvatar = new System.Windows.Forms.PictureBox();
            this.lblAnhDaiDien = new System.Windows.Forms.Label();
            this.dgvStudent = new System.Windows.Forms.DataGridView();
            this.dgvStudentID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvHoTen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvChuyenNghanh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvDiemTB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvChuyenNganh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.cmbFaculty = new System.Windows.Forms.ComboBox();
            this.txtAverageScore = new System.Windows.Forms.TextBox();
            this.txtFullName = new System.Windows.Forms.TextBox();
            this.txtStudentID = new System.Windows.Forms.TextBox();
            this.lblKhoa = new System.Windows.Forms.Label();
            this.lblDiemTB = new System.Windows.Forms.Label();
            this.lblHoTen = new System.Windows.Forms.Label();
            this.lblMaSinhVienn = new System.Windows.Forms.Label();
            this.lblThongTinSinhVien = new System.Windows.Forms.Label();
            this.lblQuanLySinhVien = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picAvatar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudent)).BeginInit();
            this.SuspendLayout();
            // 
            // picAvatar
            // 
            this.picAvatar.Location = new System.Drawing.Point(254, 467);
            this.picAvatar.Margin = new System.Windows.Forms.Padding(4);
            this.picAvatar.Name = "picAvatar";
            this.picAvatar.Size = new System.Drawing.Size(209, 98);
            this.picAvatar.TabIndex = 66;
            this.picAvatar.TabStop = false;
            // 
            // lblAnhDaiDien
            // 
            this.lblAnhDaiDien.AutoSize = true;
            this.lblAnhDaiDien.Location = new System.Drawing.Point(116, 483);
            this.lblAnhDaiDien.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAnhDaiDien.Name = "lblAnhDaiDien";
            this.lblAnhDaiDien.Size = new System.Drawing.Size(81, 16);
            this.lblAnhDaiDien.TabIndex = 65;
            this.lblAnhDaiDien.Text = "Anh dai dien";
            // 
            // dgvStudent
            // 
            this.dgvStudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvStudent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvStudentID,
            this.dgvHoTen,
            this.dgvChuyenNghanh,
            this.dgvDiemTB,
            this.dgvChuyenNganh});
            this.dgvStudent.Location = new System.Drawing.Point(501, 200);
            this.dgvStudent.Margin = new System.Windows.Forms.Padding(4);
            this.dgvStudent.Name = "dgvStudent";
            this.dgvStudent.RowHeadersWidth = 51;
            this.dgvStudent.Size = new System.Drawing.Size(885, 185);
            this.dgvStudent.TabIndex = 64;
            // 
            // dgvStudentID
            // 
            this.dgvStudentID.HeaderText = "MSSV";
            this.dgvStudentID.MinimumWidth = 6;
            this.dgvStudentID.Name = "dgvStudentID";
            this.dgvStudentID.Width = 125;
            // 
            // dgvHoTen
            // 
            this.dgvHoTen.HeaderText = "Ho ten";
            this.dgvHoTen.MinimumWidth = 6;
            this.dgvHoTen.Name = "dgvHoTen";
            this.dgvHoTen.Width = 125;
            // 
            // dgvChuyenNghanh
            // 
            this.dgvChuyenNghanh.HeaderText = "Khoa";
            this.dgvChuyenNghanh.MinimumWidth = 6;
            this.dgvChuyenNghanh.Name = "dgvChuyenNghanh";
            this.dgvChuyenNghanh.Width = 125;
            // 
            // dgvDiemTB
            // 
            this.dgvDiemTB.HeaderText = "Diem TB";
            this.dgvDiemTB.MinimumWidth = 6;
            this.dgvDiemTB.Name = "dgvDiemTB";
            this.dgvDiemTB.Width = 125;
            // 
            // dgvChuyenNganh
            // 
            this.dgvChuyenNganh.HeaderText = "ChuyenNganh";
            this.dgvChuyenNganh.MinimumWidth = 6;
            this.dgvChuyenNganh.Name = "dgvChuyenNganh";
            this.dgvChuyenNganh.Width = 125;
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(419, 624);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(4);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(100, 28);
            this.btnDelete.TabIndex = 63;
            this.btnDelete.Text = "xoa";
            this.btnDelete.UseVisualStyleBackColor = true;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(231, 624);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(4);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(100, 28);
            this.btnUpdate.TabIndex = 62;
            this.btnUpdate.Text = "Them/Sua";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // cmbFaculty
            // 
            this.cmbFaculty.FormattingEnabled = true;
            this.cmbFaculty.Items.AddRange(new object[] {
            "QLKS ",
            "CNTT"});
            this.cmbFaculty.Location = new System.Drawing.Point(287, 347);
            this.cmbFaculty.Margin = new System.Windows.Forms.Padding(4);
            this.cmbFaculty.Name = "cmbFaculty";
            this.cmbFaculty.Size = new System.Drawing.Size(160, 24);
            this.cmbFaculty.TabIndex = 61;
            // 
            // txtAverageScore
            // 
            this.txtAverageScore.Location = new System.Drawing.Point(302, 402);
            this.txtAverageScore.Margin = new System.Windows.Forms.Padding(4);
            this.txtAverageScore.Name = "txtAverageScore";
            this.txtAverageScore.Size = new System.Drawing.Size(132, 22);
            this.txtAverageScore.TabIndex = 60;
            // 
            // txtFullName
            // 
            this.txtFullName.Location = new System.Drawing.Point(274, 283);
            this.txtFullName.Margin = new System.Windows.Forms.Padding(4);
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.Size = new System.Drawing.Size(132, 22);
            this.txtFullName.TabIndex = 59;
            // 
            // txtStudentID
            // 
            this.txtStudentID.Location = new System.Drawing.Point(254, 205);
            this.txtStudentID.Margin = new System.Windows.Forms.Padding(4);
            this.txtStudentID.Name = "txtStudentID";
            this.txtStudentID.Size = new System.Drawing.Size(132, 22);
            this.txtStudentID.TabIndex = 58;
            // 
            // lblKhoa
            // 
            this.lblKhoa.AutoSize = true;
            this.lblKhoa.Location = new System.Drawing.Point(127, 350);
            this.lblKhoa.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKhoa.Name = "lblKhoa";
            this.lblKhoa.Size = new System.Drawing.Size(38, 16);
            this.lblKhoa.TabIndex = 57;
            this.lblKhoa.Text = "Khoa";
            // 
            // lblDiemTB
            // 
            this.lblDiemTB.AutoSize = true;
            this.lblDiemTB.Location = new System.Drawing.Point(116, 402);
            this.lblDiemTB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDiemTB.Name = "lblDiemTB";
            this.lblDiemTB.Size = new System.Drawing.Size(60, 16);
            this.lblDiemTB.TabIndex = 56;
            this.lblDiemTB.Text = "Diem TB";
            // 
            // lblHoTen
            // 
            this.lblHoTen.AutoSize = true;
            this.lblHoTen.Location = new System.Drawing.Point(112, 283);
            this.lblHoTen.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblHoTen.Name = "lblHoTen";
            this.lblHoTen.Size = new System.Drawing.Size(52, 16);
            this.lblHoTen.TabIndex = 55;
            this.lblHoTen.Text = "Ho Ten";
            // 
            // lblMaSinhVienn
            // 
            this.lblMaSinhVienn.AutoSize = true;
            this.lblMaSinhVienn.Location = new System.Drawing.Point(87, 205);
            this.lblMaSinhVienn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMaSinhVienn.Name = "lblMaSinhVienn";
            this.lblMaSinhVienn.Size = new System.Drawing.Size(85, 16);
            this.lblMaSinhVienn.TabIndex = 54;
            this.lblMaSinhVienn.Text = "Ma Sinh Vien";
            // 
            // lblThongTinSinhVien
            // 
            this.lblThongTinSinhVien.AutoSize = true;
            this.lblThongTinSinhVien.Location = new System.Drawing.Point(87, 161);
            this.lblThongTinSinhVien.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblThongTinSinhVien.Name = "lblThongTinSinhVien";
            this.lblThongTinSinhVien.Size = new System.Drawing.Size(127, 16);
            this.lblThongTinSinhVien.TabIndex = 53;
            this.lblThongTinSinhVien.Text = "Thong Tin Sinh Vien";
            // 
            // lblQuanLySinhVien
            // 
            this.lblQuanLySinhVien.AutoSize = true;
            this.lblQuanLySinhVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQuanLySinhVien.Location = new System.Drawing.Point(374, 98);
            this.lblQuanLySinhVien.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblQuanLySinhVien.Name = "lblQuanLySinhVien";
            this.lblQuanLySinhVien.Size = new System.Drawing.Size(272, 25);
            this.lblQuanLySinhVien.TabIndex = 52;
            this.lblQuanLySinhVien.Text = "Quan Ly Thong Tin Sinh Vien";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1473, 750);
            this.Controls.Add(this.picAvatar);
            this.Controls.Add(this.lblAnhDaiDien);
            this.Controls.Add(this.dgvStudent);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.cmbFaculty);
            this.Controls.Add(this.txtAverageScore);
            this.Controls.Add(this.txtFullName);
            this.Controls.Add(this.txtStudentID);
            this.Controls.Add(this.lblKhoa);
            this.Controls.Add(this.lblDiemTB);
            this.Controls.Add(this.lblHoTen);
            this.Controls.Add(this.lblMaSinhVienn);
            this.Controls.Add(this.lblThongTinSinhVien);
            this.Controls.Add(this.lblQuanLySinhVien);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.picAvatar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvStudent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picAvatar;
        private System.Windows.Forms.Label lblAnhDaiDien;
        private System.Windows.Forms.DataGridView dgvStudent;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvStudentID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvHoTen;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvChuyenNghanh;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvDiemTB;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvChuyenNganh;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ComboBox cmbFaculty;
        private System.Windows.Forms.TextBox txtAverageScore;
        private System.Windows.Forms.TextBox txtFullName;
        private System.Windows.Forms.TextBox txtStudentID;
        private System.Windows.Forms.Label lblKhoa;
        private System.Windows.Forms.Label lblDiemTB;
        private System.Windows.Forms.Label lblHoTen;
        private System.Windows.Forms.Label lblMaSinhVienn;
        private System.Windows.Forms.Label lblThongTinSinhVien;
        private System.Windows.Forms.Label lblQuanLySinhVien;
    }
}

