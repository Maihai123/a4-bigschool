﻿namespace maivanhai2180600353
{
    partial class FormAdd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnThoat = new System.Windows.Forms.Button();
            this.btnADD = new System.Windows.Forms.Button();
            this.cmbFaculty = new System.Windows.Forms.ComboBox();
            this.txtAVG = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.lblDtb = new System.Windows.Forms.Label();
            this.lblKhoa = new System.Windows.Forms.Label();
            this.lblTen = new System.Windows.Forms.Label();
            this.lblMS = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnThoat
            // 
            this.btnThoat.Location = new System.Drawing.Point(546, 358);
            this.btnThoat.Margin = new System.Windows.Forms.Padding(4);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(179, 37);
            this.btnThoat.TabIndex = 19;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.UseVisualStyleBackColor = true;
            // 
            // btnADD
            // 
            this.btnADD.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnADD.Location = new System.Drawing.Point(295, 358);
            this.btnADD.Margin = new System.Windows.Forms.Padding(4);
            this.btnADD.Name = "btnADD";
            this.btnADD.Size = new System.Drawing.Size(179, 37);
            this.btnADD.TabIndex = 18;
            this.btnADD.Text = "Thêm Mới";
            this.btnADD.UseVisualStyleBackColor = true;
            // 
            // cmbFaculty
            // 
            this.cmbFaculty.FormattingEnabled = true;
            this.cmbFaculty.Items.AddRange(new object[] {
            "Công Nghệ Thông Tin",
            "Ngôn Ngữ Anh",
            "Quản Trị Kinh Doanh"});
            this.cmbFaculty.Location = new System.Drawing.Point(352, 197);
            this.cmbFaculty.Margin = new System.Windows.Forms.Padding(4);
            this.cmbFaculty.Name = "cmbFaculty";
            this.cmbFaculty.Size = new System.Drawing.Size(373, 24);
            this.cmbFaculty.TabIndex = 17;
            // 
            // txtAVG
            // 
            this.txtAVG.Location = new System.Drawing.Point(352, 266);
            this.txtAVG.Margin = new System.Windows.Forms.Padding(4);
            this.txtAVG.Name = "txtAVG";
            this.txtAVG.Size = new System.Drawing.Size(289, 22);
            this.txtAVG.TabIndex = 16;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(350, 127);
            this.txtName.Margin = new System.Windows.Forms.Padding(4);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(373, 22);
            this.txtName.TabIndex = 15;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(350, 58);
            this.txtID.Margin = new System.Windows.Forms.Padding(4);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(289, 22);
            this.txtID.TabIndex = 14;
            // 
            // lblDtb
            // 
            this.lblDtb.AutoSize = true;
            this.lblDtb.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.lblDtb.Location = new System.Drawing.Point(75, 266);
            this.lblDtb.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDtb.Name = "lblDtb";
            this.lblDtb.Size = new System.Drawing.Size(94, 25);
            this.lblDtb.TabIndex = 13;
            this.lblDtb.Text = "Điểm TB";
            // 
            // lblKhoa
            // 
            this.lblKhoa.AutoSize = true;
            this.lblKhoa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.lblKhoa.Location = new System.Drawing.Point(75, 197);
            this.lblKhoa.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblKhoa.Name = "lblKhoa";
            this.lblKhoa.Size = new System.Drawing.Size(62, 25);
            this.lblKhoa.TabIndex = 12;
            this.lblKhoa.Text = "Khoa";
            // 
            // lblTen
            // 
            this.lblTen.AutoSize = true;
            this.lblTen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.lblTen.Location = new System.Drawing.Point(75, 124);
            this.lblTen.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTen.Name = "lblTen";
            this.lblTen.Size = new System.Drawing.Size(147, 25);
            this.lblTen.TabIndex = 11;
            this.lblTen.Text = "Tên Sinh Viên";
            // 
            // lblMS
            // 
            this.lblMS.AutoSize = true;
            this.lblMS.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.lblMS.Location = new System.Drawing.Point(75, 56);
            this.lblMS.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMS.Name = "lblMS";
            this.lblMS.Size = new System.Drawing.Size(172, 25);
            this.lblMS.TabIndex = 10;
            this.lblMS.Text = "Mã Số Sinh Viên";
            // 
            // FormAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnThoat);
            this.Controls.Add(this.btnADD);
            this.Controls.Add(this.cmbFaculty);
            this.Controls.Add(this.txtAVG);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.lblDtb);
            this.Controls.Add(this.lblKhoa);
            this.Controls.Add(this.lblTen);
            this.Controls.Add(this.lblMS);
            this.Name = "FormAdd";
            this.Text = "FormAdd";
         
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.Button btnADD;
        private System.Windows.Forms.ComboBox cmbFaculty;
        private System.Windows.Forms.TextBox txtAVG;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label lblDtb;
        private System.Windows.Forms.Label lblKhoa;
        private System.Windows.Forms.Label lblTen;
        private System.Windows.Forms.Label lblMS;
    }
}