﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace maivanhai2180600353
{
  
        public partial class FormAdd : Form
        {
            public string ma
            {
                get { return txtID.Text; }
                set { txtID.Text = value; }
            }
            public string ten
            {
                get { return txtName.Text; }
                set { txtName.Text = value; }
            }

            public string khoa
            {
                get { return cmbFaculty.Text; }
                set { cmbFaculty.Text = value; }
            }
            public string diem
            {
                get { return txtAVG.Text; }
                set { txtAVG.Text = value; }
            }
            public FormAdd()
            {
                InitializeComponent();
            }

            private void FormAdd_Load(object sender, EventArgs e)
            {

                cmbFaculty.SelectedIndex = 0;

            }

            private void btnThoat_Click(object sender, EventArgs e)
            {
                DialogResult h = MessageBox.Show("Bạn có muốn thoát ?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                if (h == DialogResult.Yes)
                {
                    Application.Exit();
                }
                else
                {
                    //e.Cancel = true;
                }
            }
        }
    }

