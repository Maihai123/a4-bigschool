﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace maivanhai2180600353
{
    public partial class Form1 : Form
    {


        public Form1()
        {
            InitializeComponent();
        }
        private int GetselectedRows(string ma)
        {
            dgvDanhSach.AllowUserToAddRows = false;
            for (int i = 0; i < dgvDanhSach.Rows.Count; i++)
            {
                if (dgvDanhSach.Rows[i].Cells[1].Value.ToString() == ma)
                {
                    return i;
                }
            }
            return -1;
        }
        void addSV()
        {
            FormAdd f1 = new FormAdd();

            if (f1.ShowDialog() == DialogResult.OK)
            {
                if (f1.ma == "" || f1.ten == "" || f1.diem == "")
                {
                    MessageBox.Show("Vui lòng nhập thông tin...");
                }
                int selected = GetselectedRows(f1.ma);
                if (selected == -1)
                {
                    if (float.Parse(f1.diem) <= 10 && float.Parse(f1.diem) >= 0)
                    {
                        dgvDanhSach.Rows.Add( f1.ma, f1.ten, f1.khoa, f1.diem);
                        MessageBox.Show("Thành công!!!", "Thông Báo", MessageBoxButtons.OK);
                    }
                    else
                        MessageBox.Show("Vui lòng nhập điểm (0 - 10)...", "Thông Báo", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("Mã số sinh viên trùng vui long nhập lại...", "Thông Báo", MessageBoxButtons.OK);
                }
            }
        }
        private void thêmMớiCtrlNToolStripMenuItem_Click(object sender, EventArgs e)
        {
            addSV();
        }
        void searchName()
        {
            string item = "";
            string save = tStxtSearch.Text;
            for (int i = 0; i < dgvDanhSach.Rows.Count; i++)
            {
                item = dgvDanhSach.Rows[i].Cells[1].Value.ToString();
                if (item.ToLower().Contains(save) == true)
                {
                    dgvDanhSach.Rows[i].Visible = true;
                }
                else if (item.ToUpper().Contains(save) == true)
                {
                    dgvDanhSach.Rows[i].Visible = true;
                }
                else if (item.Contains(save) == true)
                {
                    dgvDanhSach.Rows[i].Visible = true;
                }
                else
                    dgvDanhSach.Rows[i].Visible = false;

            }
        }
        private void tStxtSearch_TextChanged(object sender, EventArgs e)
        {
            searchName();
        }

        private void frmQLSV_KeyDown(object sender, KeyEventArgs e)
        {

            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.N)
            {
                addSV();
            }
        }

        private void tìmKiếmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            searchName();

        }

        private void tSbtnThem_Click(object sender, EventArgs e)
        {
            addSV();
        }

        private void frmQLSV_Load(object sender, EventArgs e)
        {
            KeyPreview = true;
        }

        private void toolStripLabel2_Click(object sender, EventArgs e)
        {

        }

        private void dgvDanhSach_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void chứcNăngToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void toolStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void tStxtSearch_Click(object sender, EventArgs e)
        {

        }

        private void tSbtnThem_Click_1(object sender, EventArgs e)
        {
            addSV();
        }

        private void tStxtSearch_Click_1(object sender, EventArgs e)
        {

        }

        private void toolStripLabel2_Click_1(object sender, EventArgs e)
        {

        }
    }
} 



